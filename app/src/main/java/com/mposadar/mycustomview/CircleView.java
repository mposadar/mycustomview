package com.mposadar.mycustomview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;

/**
 * Created by Miguel on 22/08/2016.
 */
public class CircleView extends View {

    private Paint circle, line;
    private int cx, cy, r1, r2, r3;
    private static final String N = "N";
    private static final String NNE = "NNE";
    private static final String NE = "NE";
    private static final String ENE = "ENE";
    private static final String E = "E";
    private static final String ESE = "ESE";
    private static final String SE = "SE";
    private static final String SSE = "SSE";
    private static final String S = "S";
    private static final String SSW = "SSW";
    private static final String SW = "SW";
    private static final String WSW = "WSW";
    private static final String W = "W";
    private static final String WNW = "WNW";
    private static final String NW = "NW";
    private static final String NNW = "NNW";
    // Styles and values
    private int circleColor, labelColor;
    private float windDirection;
    private AccessibilityManager accessibilityManager;

    public CircleView(Context context, AttributeSet attrs) {
        super(context, attrs);

        //get the attributes specified in attrs.xml using the name we included
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.CircleView, 0, 0);
        try {
            //get the text and colors specified using the names in attrs.xml
            circleColor = typedArray.getInteger(R.styleable.CircleView_circleColor, 0);
            labelColor = typedArray.getInteger(R.styleable.CircleView_labelColor, 0);
        } finally {
            typedArray.recycle();
        }

        circle = new Paint();
        circle.setAntiAlias(true);
        circle.setStyle(Paint.Style.STROKE);
        circle.setColor(circleColor);
        circle.setStrokeWidth(2);
        circle.setTextSize(25);
        circle.setTextAlign(Paint.Align.CENTER);

        line = new Paint();
        line.setStyle(Paint.Style.FILL_AND_STROKE);
        line.setStrokeWidth(2);
        line.setColor(Color.BLUE);

        accessibilityManager = (AccessibilityManager)
                context.getSystemService(Context.ACCESSIBILITY_SERVICE);
        if (accessibilityManager.isEnabled()) {
            sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_TEXT_CHANGED);
        }
    }

    /**
     * convert direction from "degrees" to "radians"
     * @param direction
     * @return
     */
    private float degreesToRadians(float direction) {
        return direction * (float) (Math.PI / 180);
    }

    /**
     * widthMeasureSpec	int: horizontal space requirements as imposed by the parent.
     *                       The requirements are encoded with View.MeasureSpec.
     * heightMeasureSpec int: vertical space requirements as imposed by the parent.
     *                        The requirements are encoded with View.MeasureSpec.
     * @param widthMeasureSpec
     * @param heightMeasureSpec
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        //get half of the width and height as we are working with a circle
        cx = View.MeasureSpec.getSize(widthMeasureSpec) / 2;
        cy = View.MeasureSpec.getSize(heightMeasureSpec) / 2;
        //get the radius as half of the width or height, whichever is smaller
        //subtract ten so that it has some space around it
        r1 = 0;
        if (cx > cy){
            r1 = cy - 10;
            r2 = r1 - 15;
            r3 = cy - 65;
        }
        else {
            r1 = cx - 10;
            r2 = r1 - 15;
            r3 = cx - 65;
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // first circle...
        canvas.drawCircle(cx, cy, r1, circle);
        // second circle...
        canvas.drawCircle(cx, cy, r2, circle);

        // draw a point in the middle of the circle
        canvas.drawCircle(cx, cy, 10, line);
        // Y rect
        canvas.drawLine(cx, 10, cx, this.getMeasuredHeight() - 10, circle);
        // X rect
        canvas.drawLine(10, cy, this.getMeasuredWidth() - 10, cy, circle);

        drawCompassRose(canvas, 22.5, NNE);
        drawCompassRose(canvas, 45, NE);
        drawCompassRose(canvas, 67.5, ENE);
        drawCompassRose(canvas, 112.5, ESE);
        drawCompassRose(canvas, 135, SE);
        drawCompassRose(canvas, 157.5, SSE);
        drawCompassRose(canvas, 202.5, SSW);
        drawCompassRose(canvas, 225, SW);
        drawCompassRose(canvas, 247.5, WSW);
        drawCompassRose(canvas, 295.5, WNW);
        drawCompassRose(canvas, 315, NW);
        drawCompassRose(canvas, 337.5, NNW);

        // draw line
        canvas.drawLine(
                cx,
                cy,
                (float) (cx + r1 * Math.sin(windDirection)),
                (float) (cy - r1 * Math.cos(windDirection)),
                line);
    }

    private void drawCompassRose(Canvas canvas, double degrees, String text) {
        float radians = degreesToRadians((float) degrees);
        float startX = (float) (cx + r2 * Math.sin(radians));
        float startY = (float) (cy - r2 * Math.cos(radians));
        float endX = (float) (cx + r1 * Math.sin(radians));
        float endY = (float) (cy - r1 * Math.cos(radians));

        circle.setColor(circleColor);

        canvas.drawLine(
                startX,
                startY,
                endX,
                endY,
                circle);

        circle.setColor(labelColor);
        // draw text...
        float x = (float) (cx + r3 * Math.sin(radians));
        float y = (float) (cy - r3 * Math.cos(radians));
        canvas.drawText(text, x, y, circle);
    }

    public void setWindDirection(float windDirection) {
        this.windDirection = degreesToRadians(windDirection);
        invalidate();
        requestLayout();
    }
}
